import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import keypad.Keypad;

import java.io.*;

import javax.swing.*;
import javax.swing.border.BevelBorder;


/**
 * Demonstrate use of the Keypad component.
 */
public class KeypadDemo extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTextField display;
	
	public KeypadDemo( ) {
		this.setTitle("keypad test");
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		
		// create some actions for keypad.
		// the actions just print when a key is pressed
		String [] keys = new String [] {"1", "2", "3", "4", "5",
				 "6", "7", "8", "9", ".", "0" };
		Action [] actions = new Action[keys.length];
		for(int k=0; k<keys.length; k++) {
			actions[k] = new AbstractAction(keys[k]) {

				@Override
				public void actionPerformed(ActionEvent e) {
					// AbstractAction contains a map of key-value pairs with several pre-defined keys.
					// NAME returns the name of this Action.
					String key = (String) this.getValue(NAME);
					System.out.println( key );
					append( key );
				}
			};
		}
		// make a keypad
		Keypad keypad = new Keypad(4,3);
		keypad.setKeys(actions);
		this.getContentPane().add( keypad, BorderLayout.CENTER );
		
		display = new JTextField(12);
		display.setEditable(false);
		display.setBorder( new BevelBorder(BevelBorder.LOWERED));
		display.setHorizontalAlignment(JTextField.RIGHT);
		this.getContentPane().add( display, BorderLayout.NORTH );
		this.pack();
	}
	
	private void append(String s) {
		display.setText( display.getText()+s);
	}
	
	public void run() {
		this.setVisible(true);
	}
	public static void main(String [] args) {
		Runnable demo = new Runnable() {
			public void run() {
				new KeypadDemo().run();
			}
		};
		SwingUtilities.invokeLater(demo);
	}

}
