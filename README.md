# Keypad #

This project contains a Keypad component that contains buttons for keys,
like a telephone or calculator keypad.  The keys can be anything.  You
"program" the keys using Action objects.

## Example

KeypadDemo contains complete code for demo.

```java
public class KeypadDemo extends JFrame {

    public KeypadDemo( ) {
        this.setTitle("keypad test");
        // create some actions for keypad.
	Action [] actions = new Action[3];
	actions[0] = new AbstractAction('1') {
             public void actionPerformed(ActionEvent e) {
                   System.out.println( this.getValue(NAME));
             }
	}
        actions[1] = /* similar code */;
        actions[2] = /* another action */;
        // make a keypad
        Keypad keypad = new Keypad(1,3); // 1 row, 3 columsn
        keypad.setKeys(actions);
        this.getContentPane().add( keypad );
        this.pack();
    }
```